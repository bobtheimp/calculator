# Calculator #

This example is built in a docker container, using nginx 1.17.8, PHP7.4
and with Symfony framework 5.1.

### How do I get set up? ###

To run:

- Clone the repository
- Ensure docker-compose is installed and working
- Add docker.con to your hosts file set to 172.17.0.1 (or whatever docker is
  using as the bridge IP on your installation)
- Execute "docker-compose build && docker-compose up -d" in the docker
  directory
- Go to the URL: docker.con:8000
- Enjoy the calculating goodness

### Notes and improvements ###

First off, the tests are not 100% comprehensive, I am aware of that but time 
constraints dictated the level they are at. 
Some improvements would be to check for rounding and overflow errors, and more
robust checking of the routes, but * most * common errors should be accounted
for, and the tests (52 at current count) exercise all exposed functions as well
as a black box approach.

Also, I am aware that as of 5.2,
Sensio\Bundle\FrameworkExtraBundle\Routing\AnnotatedRouteControllerLoader
is deprecated, needs updating in the Kernel.php file, but at the moment
it's simply a runtime warning.
