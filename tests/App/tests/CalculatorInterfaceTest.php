<?php
declare(strict_types=1);
/**
 * Test case for calculator URL
 */

namespace App\tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CalculatorInterfaceTest
 * @package App\tests
 */
final class CalculatorInterfaceTest extends WebTestCase
{
    /**
     * test the URL is retrieved OK
     */
    public function testCalculatorInterface(): void
    {
        $c = CalculatorInterfaceTest::createClient();
        $c->request('GET', '/');
        $this->assertEquals(200, $c->getResponse()->getStatusCode(), 'REQUEST IS : ' . $c->getRequest());
    }

    // TODO: add tests for routes
}
