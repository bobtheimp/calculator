<?php
declare(strict_types=1);
/**
 * Test case for calculateMultiply class
 */

namespace App\tests\Calculator;

use App\Service\Calculator\CalculateMultiply;
use PHPUnit\Framework\TestCase;

/**
 * Class calculateMultiplyTest
 * @package App\tests\Calculator
 *
 * @covers \App\Service\Calculator\CalculateMultiply
 * @coversDefaultClass \App\Service\Calculator\CalculateMultiply
 */
final class CalculateMultiplyTest extends TestCase
{
    private CalculateMultiply $calculator;

    /**
     * CalculateMultiplyTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculator = new CalculateMultiply();
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     *
     * @dataProvider calculateMultiplyProvider
     */
    public function testCalculateMultiply(string $a, string $b, float $result): void
    {
        $this->assertEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     *
     * @dataProvider calculateMultiplyProviderBad
     */
    public function testCalculateMultiplyBad(string $a, string $b, float $result): void
    {
        $this->assertNotEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @return array[]
     */
    public function calculateMultiplyProvider(): array
    {
        return [
            ['0', '0', 0],
            ['10', '10', 100],
            ['9', '9', 81],
            ['6', '6', 36]
        ];
    }

    /**
     * @return array[]
     */
    public function calculateMultiplyProviderBad(): array
    {
        return [
            ['0', '0', 7],
            ['10', '10', 4],
            ['9', '9', 9],
            ['6', '6', 6]
        ];
    }

    // TODO: add tests for rounding and overflow errors
}
