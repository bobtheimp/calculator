<?php
declare(strict_types=1);
/**
 * Test case for base calculator class
 */

namespace App\tests\Calculator;

use App\Service\Calculator\CalculatorFactory;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Class CalculatorBaseTest
 * @package App\tests\Calculator
 */
class CalculatorBaseTest extends TestCase
{
    private CalculatorFactory $calculatorFactory;
    private string $a;
    private string $b;

    /**
     * CalculatorBaseTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculatorFactory = new CalculatorFactory();
    }

    /**
     * @param $operation
     * @param $valuesArray
     * @dataProvider baseValuesProviderBad
     */
    public function testBaseSetValuesException($operation, $valuesArray): void
    {
        $object = $this->setupTest($operation, $valuesArray);

        echo "Testing arguments a={$this->a}, b={$this->b} for InvalidArgumentsException.".PHP_EOL;

        $this->expectException(InvalidArgumentException::class);
        $object->setValues(['a' => $this->a, 'b' => $this->b]);
    }

    /**
     * @param $operation
     * @param $valuesArray
     * @dataProvider baseValuesProvider
     */
    public function testBaseSetValuesBadParameter($operation, $valuesArray): void
    {
        $object = $this->setupTest($operation, $valuesArray);

        echo "Testing arguments x={$this->a}, y={$this->b} for InvalidArgumentsException.".PHP_EOL;

        $this->expectException(InvalidArgumentException::class);
        $object->setValues(['x' => $this->a, 'y' => $this->b]);
    }

    /**
     * @param $operation
     * @param $valuesArray
     * @return object
     */
    private function setupTest($operation, $valuesArray): object
    {
        list($this->a, $this->b) = $valuesArray;
        return $this->setOperation($operation);
    }

    /**
     * @param $operation
     * @return object
     */
    private function setOperation($operation): object
    {
        return $this->calculatorFactory->create($operation);
    }

    /**
     * @return string[][]
     */
    public function baseValuesProviderBad(): array
    {
        return [
            ['add', ['g', '10']],
            ['divide', ['10', 'r']],
            ['multiply', ['@', '2']],
            ['subtract', ['5', 'bob']],
        ];
    }

    /**
     * @return string[][]
     */
    public function baseValuesProvider(): array
    {
        return [
            ['add', ['10', '10']],
            ['divide', ['10', '2']],
            ['multiply', ['5', '2']],
            ['subtract', ['5', '15']],
        ];
    }
}