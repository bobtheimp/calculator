<?php
declare(strict_types=1);
/**
 * Test case for calculateAdd class
 */

namespace App\tests\Calculator;

use App\Service\Calculator\CalculateAdd;
use PHPUnit\Framework\TestCase;

/**
 * Class calculateAddTest
 * @package App\tests\Calculator
 *
 * @covers \App\Service\Calculator\CalculateAdd
 * @coversDefaultClass \App\Service\Calculator\CalculateAdd
 */
final class CalculateAddTest extends TestCase
{
    private CalculateAdd $calculator;

    /**
     * CalculateAddTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculator = new CalculateAdd();
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     *
     * @dataProvider calculateAddProvider
     */
    public function testCalculateAdd(string $a, string $b, float $result): void
    {
        echo "Testing addition equals for $a + $b = $result.".PHP_EOL;
        $this->assertEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     *
     * @dataProvider calculateAddBadProvider
     */
    public function testCalculateAddBad(string $a, string $b, float $result): void
    {
        echo "Testing addition NOT equals for $a + $b = $result.".PHP_EOL;
        $this->assertNotEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @return array[]
     */
    public function calculateAddProvider(): array
    {
        return [
            ['0', '0', 0],
            ['10', '10', 20],
            ['4', '5', 9],
            ['492', '553', 1045]
        ];
    }

    /**
     * @return array[]
     */
    public function calculateAddBadProvider(): array
    {
        return [
            ['0', '0', 6],
            ['10', '10', 330],
            ['4', '5', 0],
            ['492', '553', 45]
        ];
    }

    // TODO: add tests for overflow errors
}
