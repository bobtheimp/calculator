<?php
declare(strict_types=1);
/**
 * Test case for calculateSubtract class
 */

namespace App\tests\Calculator;

use App\Service\Calculator\CalculateSubtract;
use PHPUnit\Framework\TestCase;

/**
 * Class calculateSubtractTest
 * @package App\tests\Calculator
 *
 * @covers \App\Service\Calculator\CalculateSubtract
 * @coversDefaultClass \App\Service\Calculator\CalculateSubtract
 */
final class CalculateSubtractTest extends TestCase
{
    private CalculateSubtract $calculator;

    /**
     * CalculateSubtractTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculator = new CalculateSubtract();
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     *
     * @dataProvider calculateSubtractProvider
     */
    public function testCalculateSubtract(string $a, string $b, float $result): void
    {
        $this->assertEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     *
     * @dataProvider calculateSubtractProviderBad
     */
    public function testCalculateSubtractBad(string $a, string $b, float $result): void
    {
        $this->assertNotEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @return array[]
     */
    public function calculateSubtractProvider(): array
    {
        return [
            ['0', '0', 0],
            ['10', '5', 5],
            ['100', '50', 50],
            ['463', '231', 232]
        ];
    }

    /**
     * @return array[]
     */
    public function calculateSubtractProviderBad(): array
    {
        return [
            ['0', '0', 4],
            ['10', '5', 64],
            ['100', '50', 6],
            ['463', '231', 3]
        ];
    }

    // TODO: add tests for rounding and overflow errors
}
