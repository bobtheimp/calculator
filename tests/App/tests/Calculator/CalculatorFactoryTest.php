<?php
declare(strict_types=1);
/**
 * Test case for calculator operation class factory
 */

namespace App\tests\Calculator;

use App\Service\Calculator\CalculateAdd;
use App\Service\Calculator\CalculateDivide;
use App\Service\Calculator\CalculateMultiply;
use App\Service\Calculator\CalculateSubtract;
use App\Service\Calculator\CalculatorFactory;
use InvalidArgumentException;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;

/**
 * Class CalculatorFactoryTest
 * @package App\tests\Calculator
 *
 * @covers \App\Service\Calculator\CalculatorFactory
 * @coversDefaultClass \App\Service\Calculator\CalculatorFactory
 */
final class CalculatorFactoryTest extends TestCase
{
    private CalculatorFactory $calculatorFactory;

    /**
     * CalculatorFactoryTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculatorFactory = new CalculatorFactory();
    }

    /**
     * @param $operation
     * @param $class
     *
     * @dataProvider factoryTestProvider
     */
    public function testCreateFactory($operation, $class): void
    {
        echo "Testing operation '$operation' instance of class '$class'.".PHP_EOL;

        $object = $this->calculatorFactory->create($operation);
        $this->assertInstanceOf($class, $object);
    }

    /**
     * @param $operation
     * @param $class
     *
     * @dataProvider factoryTestProviderBad
     */
    public function testCreateFactoryBad($operation, $class): void
    {
        echo "Testing operation '$operation' NOT instance of class '$class'.".PHP_EOL;

        $object = $this->calculatorFactory->create($operation);
        $this->assertNotInstanceOf($class, $object);
    }

    /**
     * @param $operation
     * @param $class
     *
     * @dataProvider factoryTestProviderUnsupportedOperation
     */
    public function testUnsupportedOperation($operation, $class): void
    {
        echo "Testing unsupported operation '$operation' with class '$class'.".PHP_EOL;

        $this->expectException(InvalidArgumentException::class);
        $object = $this->calculatorFactory->create($operation);
    }

    /**
     * @param $operation
     * @param $class
     *
     * @dataProvider factoryTestProviderInvalidClass
     */
    public function testInvalidClass($operation, $class): void
    {
        echo "Testing invalid class '$class' for operation '$operation'.".PHP_EOL;

        $this->expectException(ExpectationFailedException::class);
        $object = $this->calculatorFactory->create($operation);
        $this->assertInstanceOf($class, $object);
    }

    /**
     * @return string[][]
     */
    public function factoryTestProvider(): array
    {
        return [
            ['add', CalculateAdd::class],
            ['subtract', CalculateSubtract::class],
            ['multiply', CalculateMultiply::class],
            ['divide', CalculateDivide::class]
        ];
    }

    /**
     * @return string[][]
     */
    public function factoryTestProviderBad(): array
    {
        return [
            ['subtract', CalculateAdd::class],
            ['multiply', CalculateSubtract::class],
            ['divide', CalculateMultiply::class],
            ['add', CalculateDivide::class]
        ];
    }

    /**
     * @return string[][]
     */
    public function factoryTestProviderUnsupportedOperation(): array
    {
        return [
            ['sin', CalculateAdd::class]
        ];
    }

    /**
     * @return string[][]
     */
    public function factoryTestProviderInvalidClass(): array
    {
        return [
            ['add', CalculateSubtract::class]
        ];
    }
}
