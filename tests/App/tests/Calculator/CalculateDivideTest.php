<?php
declare(strict_types=1);
/**
 * Test case for calculateDivide class
 */

namespace App\tests\Calculator;

use App\Service\Calculator\CalculateDivide;
use DivisionByZeroError;
use PHPUnit\Framework\TestCase;

/**
 * Class calculateDivideTest
 * @package App\tests\Calculator
 *
 * @covers \App\Service\Calculator\CalculateDivide
 * @coversDefaultClass \App\Service\Calculator\CalculateDivide
 */
class CalculateDivideTest extends TestCase
{
    private CalculateDivide $calculator;

    /**
     * CalculateDivideTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->calculator = new CalculateDivide();
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     * @dataProvider calculateDivideProvider
     */
    public function testCalculateDivide(string $a, string $b, float $result): void
    {
        echo "Testing divide equals for $a / $b = $result.".PHP_EOL;
        $this->assertEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    /**
     * @param string $a
     * @param string $b
     * @param float $result
     * @dataProvider calculateDivideProviderBad
     */
    public function testCalculateDivideBad(string $a, string $b, float $result): void
    {
        echo "Testing divide NOT equals for $a / $b = $result.".PHP_EOL;
        $this->assertNotEquals($result, $this->calculator->setValues(['a' => $a, 'b' => $b])->calculate());
    }

    public function testCalculateDivideByZero(): void
    {
        echo 'Testing for divide by zero.'.PHP_EOL;
        $this->expectException(DivisionByZeroError::class);
        $this->calculator->setValues(['a' => '10', 'b' => '0'])->calculate();
    }

    /**
     * @return array[]
     */
    public function calculateDivideProvider(): array
    {
        return [
            ['0', '1', 0],
            ['100', '10', 10],
            ['36', '6', 6],
            ['81', '9', 9]
        ];
    }

    /**
     * @return array[]
     */
    public function calculateDivideProviderBad(): array
    {
        return [
            ['0', '1', 4],
            ['100', '10', 12],
            ['36', '6', 77],
            ['81', '9', 3]
        ];
    }

    // TODO: add tests for rounding and overflow errors
}
