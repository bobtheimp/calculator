<?php
declare(strict_types=1);
/**
 * Show the calculator interface
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalculatorInterfaceController
 * @package App\Controller
 */
class CalculatorInterfaceController extends AbstractController
{
    /**
     * @return Response
     * @Route("/", name="show_calculator")
     */
    public function showCalculator(): Response
    {
        return $this->render('calculator/calculator_interface.twig');
    }
}
