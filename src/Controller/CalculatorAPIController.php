<?php
declare(strict_types=1);
/**
 * preform API function calls for the calculator, using factory patterns to determine the relevant operation class
 */

namespace App\Controller;

use App\Service\Calculator\CalculatorFactory;
use DivisionByZeroError;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalculatorAPIController
 * @package App\Controller
 */
class CalculatorAPIController extends AbstractController
{
    /** @var CalculatorFactory $calculatorFactory */
    protected CalculatorFactory $calculatorFactory;

    /**
     * CalculatorAPIController constructor.
     * @param CalculatorFactory $calculatorFactory
     */
    public function __construct(CalculatorFactory $calculatorFactory)
    {
        $this->calculatorFactory = $calculatorFactory;
    }

    /**
     * @param string $operation
     * @param string $a
     * @param string $b
     * @return Response
     *
     * This is cheating, but allows for empty first or second numbers in the frontend in case the javascript
     * doesn't catch it first
     * @Route("/api/{operation}/{a}/{b}", defaults={"a"=0,"b"=0})
     * @Route("/api/{operation}//{b}", defaults={"a"=0,"b"=0})
     * @Route("/api/{operation}/{a}/", defaults={"a"=0,"b"=0})
     */
    public function operation(string $operation, string $a, string $b): Response
    {
        // TODO: incorporate an error box on the screen to show the exceptions, rather than the result box
        try {
            $result = $this->calculatorFactory->create($operation)->setValues(['a' => $a, 'b' => $b])->calculate();
        } catch (InvalidArgumentException | DivisionByZeroError $exception) {
            $result = $exception->getMessage();
        }

        return $this->json(['result' => $result]);
    }
}
