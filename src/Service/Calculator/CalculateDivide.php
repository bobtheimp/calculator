<?php
declare(strict_types=1);
/**
 * Implement the Division service class
 */

namespace App\Service\Calculator;

use DivisionByZeroError;

/**
 * Class calculateDivide
 * @package App\Service\Calculator
 */
class CalculateDivide extends CalculatorBase implements CalculatorInterface
{
    /**
     * @return float
     */
    public function calculate(): float
    {
        $this->checkDivideByZero();
        return $this->a / $this->b;
    }

    private function checkDivideByZero(): void
    {
        // loose comparison so as not to lose any precision when comparing to 0
        if (0 == $this->b) {
            throw new DivisionByZeroError('Divide by zero!');
        }
    }
}
