<?php
declare(strict_types=1);
/**
 * Implement the Addition service class
 */

namespace App\Service\Calculator;

/**
 * Class calculateAdd
 * @package App\Service\Calculator
 */
class CalculateAdd extends CalculatorBase implements CalculatorInterface
{
    /**
     * @return float
     */
    public function calculate(): float
    {
        return $this->a + $this->b;
    }
}
