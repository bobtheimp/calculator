<?php
declare(strict_types=1);
/**
 * Define the "abstract" method to use for the operation classes
 */

namespace App\Service\Calculator;

/**
 * Interface CalculatorInterface
 * @package App\Service\Calculator
 */
interface CalculatorInterface
{
    /**
     * @return float
     */
    public function calculate(): float;
}
