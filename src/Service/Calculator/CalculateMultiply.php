<?php
declare(strict_types=1);
/**
 * Implement the Multiplication service class
 */

namespace App\Service\Calculator;

/**
 * Class calculateMultiply
 * @package App\Service\Calculator
 */
class CalculateMultiply extends CalculatorBase implements CalculatorInterface
{
    /**
     * @return float
     */
    public function calculate(): float
    {
        return $this->a * $this->b;
    }
}
