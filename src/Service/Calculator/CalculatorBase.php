<?php
declare(strict_types=1);
/**
 * provide common functions for the calculation classes
 */

namespace App\Service\Calculator;

use InvalidArgumentException;

/**
 * Class CalculatorBase
 * @package App\Service\Calculator
 */
class CalculatorBase
{
    protected float $a;
    protected float $b;

    public function __construct()
    {
        $this->initialise();
    }

    protected function initialise(): void
    {
        $this->a = 0;
        $this->b = 0;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setValues(array $values): self
    {
        return $this->checkParametersAndSetVariables($values);
    }

    /**
     * @param array $parameters
     * @return $this
     */
    protected function checkParametersAndSetVariables(array $parameters): self
    {
        // TODO: SOLID!
        foreach ($parameters as $variable => $value) {
            if (false === $this->checkVariable($variable)) {
                throw new InvalidArgumentException("Parameter variable '$variable' unrecognised!");
            }

            if (false === $this->checkValue($value)) {
                throw new InvalidArgumentException("Value '$value' passed for '$variable' is NOT numeric!");
            }

            $this->$variable = (float)$value;
        }

        return $this;
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function checkValue(string $value): bool
    {
        return is_numeric($value);
    }

    /**
     * @param string $variable
     * @return bool
     */
    protected function checkVariable(string $variable): bool
    {
        return isset($this->$variable);
    }
}
