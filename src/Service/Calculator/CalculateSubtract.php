<?php
declare(strict_types=1);
/**
 * Implement the Subtraction service class
 */

namespace App\Service\Calculator;

/**
 * Class calculateSubtract
 * @package App\Service\Calculator
 */
class CalculateSubtract extends CalculatorBase implements CalculatorInterface
{
    /**
     * @return float
     */
    public function calculate(): float
    {
        return $this->a - $this->b;
    }
}
