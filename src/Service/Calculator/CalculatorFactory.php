<?php
declare(strict_types=1);
/**
 * factory to create the appropriate class object for the desired operation
 */

namespace App\Service\Calculator;

use InvalidArgumentException;

/**
 * Class CalculatorFactory
 * @package App\Service\Calculator
 */
class CalculatorFactory
{
    /** @var array|string[] $operationClasses */
    protected array $operationClasses = [
        'add' => CalculateAdd::class,
        'subtract' => CalculateSubtract::class,
        'multiply' => CalculateMultiply::class,
        'divide' => CalculateDivide::class
    ];

    /**
     * @param string $operation
     * @return CalculatorInterface
     */
    public function create(string $operation): Object
    {
        if (!array_key_exists($operation, $this->operationClasses)) {
            throw new InvalidArgumentException("Operation '$operation' not recognised!");
        }

        if (!class_exists($this->operationClasses[$operation])) {
            throw new InvalidArgumentException("Class for operation '$operation' does not exist!");
        }

        return new $this->operationClasses[$operation];
    }
}
