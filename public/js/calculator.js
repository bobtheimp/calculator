$(document).ready(function() {
    $('#calculate').on('click', function(event) {
        event.preventDefault();
        $('#calculate').attr("disabled", true);

        $.ajax({
            method: 'GET',
            url: getURL()
        }).done(function(payload){
            displayResult(payload);
        })

        $('#calculate').attr("disabled", false);
    });

    function getURL()
    {
        let operation = $('#operation').val();
        let a = ($('#first').val()) ? $('#first').val() : 0;
        let b = ($('#second').val()) ? $('#second').val() : 0;
        return '/api/'+operation+'/'+a+'/'+b;
    }

    function clearResult()
    {
        $('#result').html('');
    }

    function displayResult(payload)
    {
        clearResult();
        $('#result').val(payload.result);
    }
});